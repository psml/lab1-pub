{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float: right; width: 50%;\">\n",
    "    <p style=\"margin: 0; padding-top: 22px; text-align:right;\">Laboratory sessions</p>\n",
    "    <p style=\"margin: 0; text-align:right;\">Privacy and Security with Machine Learning (PSML) - 2023/24</p>\n",
    "    <p style=\"margin: 0; text-align:right; padding-button: 100px;\">School of Informatics - University of Edinburgh</p>\n",
    "</div>\n",
    "\n",
    "</div>\n",
    "<div style=\"width: 100%; clear: both;\">\n",
    "<div style=\"width:100%;\">&nbsp;</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": "true"
   },
   "source": [
    "# Lab 1\n",
    "\n",
    "The goal of the lab sessions is for you to gain hands-on experience with selected topics from the course. The lab sessions are not marked, but the final exam may include questions related to the topics of the lab sessions. The work you put in during the labs is for your own benefit.\n",
    "\n",
    "The sessions are mostly self-contained, but basic Python programming knowledge is required. You can refresh your Python skills or learn the basics by taking this free online course: https://developers.google.com/edu/python\n",
    "\n",
    "To complete this lab, you must enter solutions in the provided blank code cells of this Jupyter notebook. We will provide the answers and associated source code in a separate notebook at the end of each lab session.\n",
    "\n",
    "When we ask you to implement a method, we provide the method signature (name and parameters) along with optional type hints. However, these are not intended to be prescriptive: feel free to use your own signatures and ignore the hints. You can also define additional helper methods and call them from the main methods.\n",
    "\n",
    "The `intermediate` folder in this directory contains intermediate results that you can use to skip ahead if you get blocked. The cells that start by \"CHECKPOINT\" allow you to load a snapshot of the data as if you had completed all the previous steps correclty.\n",
    "\n",
    "## Table of Contents\n",
    " <p><div class=\"lev1\"><a href=\"#Introduction-to-Website-Fingerprinting\"><span class=\"toc-item-num\">1&nbsp;&nbsp;</span>Introduction to Website Fingerprinting</a></div><div class=\"lev2\"><a href=\"#Traffic-analysis\"><span class=\"toc-item-num\">1.1&nbsp;&nbsp;</span>Traffic analysis</a></div><div class=\"lev3\"><a href=\"#Data-description\"><span class=\"toc-item-num\">1.1.1&nbsp;&nbsp;</span>Data description</a></div><div class=\"lev1\"><a href=\"#Exercises\"><span class=\"toc-item-num\">2&nbsp;&nbsp;</span>Exercises</a></div><div class=\"lev2\"><a href=\"#Website-Fingerprinting\"><span class=\"toc-item-num\">2.1&nbsp;&nbsp;</span>Website Fingerprinting</a></div><div class=\"lev2\"><a href=\"#Feature-importance\"><span class=\"toc-item-num\">2.2&nbsp;&nbsp;</span>Feature importance</a></div><div class=\"lev2\"><a href=\"#Attack-evaluation-on-Tor\"><span class=\"toc-item-num\">2.3&nbsp;&nbsp;</span>Attack evaluation on Tor</a></div><div class=\"lev2\"><a href=\"#Traffic-volume-features\"><span class=\"toc-item-num\">2.4&nbsp;&nbsp;</span>Traffic volume features</a></div><div class=\"lev2\"><a href=\"#Attack-success-per-website\"><span class=\"toc-item-num\">2.5&nbsp;&nbsp;</span>Attack success per website</a></div><div class=\"lev2\"><a href=\"#Padding-schemes\"><span class=\"toc-item-num\">2.6&nbsp;&nbsp;</span>Padding schemes</a></div><div class=\"lev2\"><a href=\"#Overhead-metrics\"><span class=\"toc-item-num\">2.7&nbsp;&nbsp;</span><em>Overhead </em> metrics</a></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Website Fingerprinting\n",
    "\n",
    "---\n",
    "\n",
    "\n",
    "Traffic analysis\n",
    "-----------------------------------------------------\n",
    "\n",
    "We captured network traffic generated by visiting a number of web pages. The selected pages were the home pages of the top 10 most popular websites according to the [SimilarWeb](https://www.similarweb.com/top-websites/) ranking. Each page was visited 100 times using Firefox and 100 times using the Tor Browser (a hardened Firefox fork) through the Tor network.\n",
    "\n",
    "The traffic traces were processed to include only the relevant network packets. All other traffic (e.g., from other applications) was discarded. In addition, retransmissions and empty TCP packets (e.g., ACKs) were removed. Finally, we extracted the payload length and timestamp from each remaining packet. The actual payload content was not useful for traffic analysis, as it was encrypted. Recall that in Website Fingerprinting, we assume the adversary is local and passive, and thus unable to decrypt the intercepted communications.\n",
    "\n",
    "### Data description\n",
    "\n",
    "The datasets can be found in the `data` folder in the directory of this notebook. There are two dataset files: `web_traffic.csv` and `tor_traffic.csv`, for web and Tor traffic, respectively.\n",
    "\n",
    "Each row of the CSV files corresponds to a single TCP packet. The columns of the CSV file are the traffic features described below:\n",
    "\n",
    " | Name | Description |\n",
    " | --- | --- |\n",
    " | `website` | Name of the website. |\n",
    " | `instance` | Visit instance identifier, unique within the website. |\n",
    " | `timestamp` | UNIX timestamp when the packet was captured. |\n",
    " | `length` | Packet payload length in bytes. Sign encodes direction: negative for incoming and positive for outgoing packets. |\n",
    " \n",
    " ---\n",
    " \n",
    "We refer to the traffic generated by a single visit as a traffic \"trace\" or \"capture\". The `instance` identifier indexes the traces for a website (there were 100 visits to each website, so we have 100 traces per website). A trace is uniquely identified by the tuple (`website`, `instance`).\n",
    "\n",
    "The domains of the visited websites are:\n",
    "\n",
    "  - bing.com\n",
    "  - wikipedia.org\n",
    "  - nytimes.com\n",
    "  - youtube.com\n",
    "  - amazon.com\n",
    "  - netflix.com\n",
    "  - reddit.com\n",
    "  - vk.com\n",
    "  - twitter.com\n",
    "  - panda.tv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to load both datasets into memory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "\n",
    "# fix numpy's rng seed for reproducibility\n",
    "np.random.seed(0) \n",
    "\n",
    "\n",
    "# shortcut to get website labels from dataframe\n",
    "def labels(df):\n",
    "    return df.index.get_level_values('website')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load web data\n",
    "web = pd.read_csv('data/web_traffic.csv')\n",
    "tor = pd.read_csv('data/tor_traffic.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "web.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tor.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises\n",
    "\n",
    "## Website Fingerprinting\n",
    "\n",
    "To begin, you will implement a simple website fingerprinting attack. As explained during the course, website fingerprinting involves an adversary observing network traffic at an intermediate point (e.g., an ISP router) between the client and web server. In the case of Tor, the adversary is situated between the client and the entry node into the Tor network, as shown in the following figure:\n",
    "\n",
    "\n",
    "<center>\n",
    "<img src=\"https://dl.acm.org/cms/asset/c0e98571-275b-4504-968c-d712bf19c524/3243734.3278493.key.jpg\" alt=\"WF Threat Model\" style=\"width: 500px;\"></img></br>Fig. 1. Website Fingerprinting threat model.</center></br>\n",
    "\n",
    "Most _website fingerprinting_ attacks use ML classifiers to infer the page that was visited from the encrypted network traffic. The classifiers use relevant traffic properties as features and page names as labels.\n",
    "\n",
    "\n",
    "A website fingerprinting attack involves the following steps:\n",
    "\n",
    "1. **Data collection**. The adversary collects traffic data through their own setting, set up to immitate that of the victim's. For instance, if the victim uses Tor, the adversary collects data through Tor. You can skip this step as we already provide the traffic datasets. Note that the provided datasets represent two different scenarios (and therefore two different attacks).\n",
    "2. **Feature extraction**. The adversary extracts from the data traffic characteristics that identify the web pages. A characteristic is identifying if it is unique to a page: it takes different values for visits to different pages, but similar values for visits to the same page.\n",
    "3. **Training**. The adversary then trains a model with the extracted characteristics as features, along with class labels representing page names. The adversary would have labeled these classes in step 1.\n",
    "4. **Deployment**. Finally, the adversary applies the model to an unlabeled trace collected from the victim to infer which page it belongs to. For evaluation, you will perform this step on a held-out test set to measure attack effectiveness.\n",
    "\n",
    "\n",
    "You can find more details about the _website fingerprinting_ methodology in the lecture slides and the recommended readings.\n",
    "\n",
    "**Exercise 1**\n",
    "\n",
    "**a.** Extract the following set of features *for each trace* from the `web` dataset:\n",
    "\n",
    " | Name | Description |\n",
    " | --- | --- |\n",
    " | `max_in_size` | Maximum length of an incoming packet. |\n",
    " | `min_in_size` | Minimum length of an incoming packet. |\n",
    " | `max_out_size` | Maximum length of an outgoing packet. |\n",
    " | `min_out_size` | Minimum length of an outgoing packet. |\n",
    " | `mean_in_size` | Average length of an incoming packet. |\n",
    " | `mean_out_size` | Average length of an outgoing packet. |\n",
    "\n",
    "- Use the Pandas `agg` method on the DataFrame to apply the feature extraction methods. See the following example that extracts the maximum size of a trace:\n",
    "\n",
    "```py\n",
    "# Group by trace\n",
    "groupby_trace = web.groupby(['website', 'instance'])\n",
    "\n",
    "# Define the feature methods. The syntax is: `\"<feature name>\": (\"<column name>\", <method>)`\n",
    "# That is, apply <method> on the column with name <column name> and name the result as <feature name>\n",
    "feature_methods = {\"max_size\": ('length', lambda x: x.max())}\n",
    "\n",
    "# Apply the methods to extract the features\n",
    "web_features = groupby_trace.agg(**feature_methods)\n",
    "    \n",
    "```\n",
    "\n",
    "Alternatively, we could have passed the method `max` directly:\n",
    "\n",
    "```py\n",
    "features_methods = {\"max_size\": ('length', max)}\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    " # TODO: extract features from the data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CHECKPOINT: load the intermediate result\n",
    "web_features = pd.read_csv('intermediate/ex1_web_features.csv', index_col=False)\n",
    "web_features.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**b.** Next you need to use the feature vectors that you obtained to train a **decision tree**.\n",
    "\n",
    "Use the `sklearn` Python module. The class you will need is [`DecisionTreeClassifier`](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html). There are many examples on how to train and evaluate a classification model in the `sklearn` documentation. For an overview on model training with `sklearn` see the following tutorial:\n",
    "\n",
    "https://scikit-learn.org/stable/tutorial/basic/tutorial.html\n",
    "\n",
    "We recommend that you use `sklearn`'s [`cross_val_score`](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.cross_val_score.html) method which applies [k-fold cross-validation](https://scikit-learn.org/stable/modules/cross_validation.html). k-fold cross-validation makes `k` disjoint partitions of the dataset to train and evaluate the model on different samples of the data. Therefore, with k-fold cross-validation, we obtain a better measurement of the generalization ability of the classifier than if we used a fixed set of data. The parameter values you must pass to `cross_val_score` are:\n",
    "\n",
    " | Name | Description |\n",
    " | --- | --- |\n",
    " | `estimator` | The classifier instance. |\n",
    " | `X` | Dataframe holding the feature set and excluding the labels (website names). Each row of `X` corresponds to the feature values that we extracted from the traffic trace. We call such a vector of features `training instance` in the context of machine learning (not to be confused with the webpage visit `instance`).|\n",
    " | `y`| List of labels. The $i$-th element of `y` is the name of the website represented by the feature vector in the $i$-th row of `X`.\n",
    " | `cv` | Number of folds (`k`). Use 5 folds.|\n",
    " | `scoring` | The metric to measure the classifier's performance. Use `recall_macro`.|\n",
    " | `random_state`| The seed of the random number generator. Set it to `0`. |\n",
    " ------\n",
    " \n",
    " [\"Recall\"](https://en.wikipedia.org/wiki/Precision_and_recall) is the ratio of true positives over the total positive predictions and measures the probability of correctly identifying a web visit. After training, display the average of the list of Recall values returned by the cross-validation method.\n",
    "\n",
    "Whenever you use a method with randomness, remember to seed the random number generator with `0` using the `random_state` parameter when available (e.g., when instantiating the class `DecisionTreeClassifier`).\n",
    "\n",
    "You can use the `labels` method defined in the first cell of this notebook to extract the labels from the dataframe with the features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn import tree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: train and test the decision tree with 5-fold cross-validation. Report the mean Recall:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Feature importance\n",
    "\n",
    "**Exercise 2**\n",
    "\n",
    "Training a decision tree involves splitting the training data based on feature values——each split is a \"decision\" in the tree. The features to split the data are chosen to maximize a function that measures __how well the feature separates the classes__. An advantage of this method to construct the tree is that the order of the features in the decision tree can be interpreted as a measure of the relative 'importance' of the feature in solving the classification problem——in our case, how effective the feature is at distinguishing a page from its traffic features. Specifically, the root of the tree indicates the feature that _reveals the most information_ about the web page.\n",
    "\n",
    "At testing time, to infer a test instance's class (or label), we traverse the trained decision tree by checking the instance's feature values at each decision node until we reach a leaf node, which provides the predicted label.\n",
    "\n",
    "Use the sklearn method: [`plot_tree`](https://scikit-learn.org/stable/modules/generated/sklearn.tree.plot_tree.html) to visualize the decision tree. Note which features are the most important (in the first levels of the tree).\n",
    "\n",
    "In order to fit the decision tree inside the cell, limit the number of levels in the tree to *three levels*. To do this, instantiate a new tree with `max_depth=3` and train it. Additionally, you can use the `font_size` parameter and the figure size (`figsize`) to further adjust figure size and fit the tree within the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(22, 10))\n",
    "\n",
    "# TODO: plot a tree with three levels\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Attack evaluation on Tor\n",
    "\n",
    "**Exercise 3**\n",
    "\n",
    "**a.** Extract the features from the Tor dataset and use them to train and evaluate another decision tree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: extract features from tor dataset:\n",
    "\n",
    "# TODO: train and test the decision tree with 5-fold cross-validation. Report the mean Recall:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CHECKPOINT: load the intermediate result\n",
    "tor_features = pd.read_csv('intermediate/ex3_tor_features.csv', index_col=False)\n",
    "tor_features.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**b.** Next try to find out why the attack is less effective against Tor. To do so, plot the histograms of the `length` feature for both datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: plot histograms for `tor` and `web` lengths.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Traffic volume features\n",
    "\n",
    "**Exercise 4**\n",
    "\n",
    "**a.** Add the following features to the feature set obtained in the previous exercises. Now repeat the previous steps: extract the features and train yet another decision tree.\n",
    "\n",
    " | Name | Description |\n",
    " | --- | --- |\n",
    " | `total_in_volume` | Total volume of incoming traffic in bytes. |\n",
    " | `total_out_volume` | Total volume of outgoing traffic in bytes. |\n",
    " | `packet_count` | Total number of packets. |\n",
    " | `load_time` | Duration of the transmission. |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: extract the union of old and new features from the web data\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**b.** Apply the attack again on the new traffic dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: train and test the decision tree with 5-fold cross-validation. Report the mean Recall:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CHECKPOINT: load the intermediate result\n",
    "web_features2 = pd.read_csv('intermediate/ex3_tor_features.csv', index_col=False)\n",
    "web_features2.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**c.** Similarly, extract the new volume features from the Tor dataset and apply the attack."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: extract new features from the tor dataset:\n",
    "\n",
    "# TODO: train and test the decision tree with 5-fold cross-validation. Report the mean Recall:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CHECKPOINT: load the intermediate result\n",
    "tor_features2 = pd.read_csv('intermediate/ex4_tor_features2.csv', index_col=False)\n",
    "tor_features2.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**d.** Plot the new decision tree and note the most important features in the Tor dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the width and height of the figure\n",
    "fig = plt.figure(figsize=(22, 10))\n",
    "\n",
    "# TODO: plot the tree:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Attack success per website\n",
    "\n",
    "**Exercise 5**\n",
    "\n",
    "The success of the website fingerprinting attacks varies across websites. In this exercise you will give a breakdown of the attack's Recall **per website**.\n",
    "\n",
    "Do not use the method `cross_val_score`. You should use `sklearn`'s [`train_test_split`](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html) instead to obtain one single split of the data into training and testing. Then, call the classifier's [`fit`](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier.fit) and [`predict`](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier.predict) methods to train the decision tree and obtain the individual predictions on the test set. Use the classifier predictions and the true labels on the test instances to call the [`classification_report`](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.classification_report.html) method. This method will caculate various metrics to measure the quality of the classifier's predictions (Recall=True Positive Rate) for each website.\n",
    "\n",
    "Call `train_test_split` with the following arguments:\n",
    "\n",
    "| Name | Description |\n",
    "| -- | -- |\n",
    "| `X`| Dataframe with the features. |\n",
    "| `train_size`| Ratio of the training set over the total. Use `0.75`. |\n",
    "| `stratify` | It ensures the same number of instances across pages in the trainig and test sets. You must give the set of labels that corresponds to `X`. |\n",
    "| `random_state`| Set it to `0`. |\n",
    "\n",
    "You can use the parameter `output_dict` of `classification_report` to obtain a dictionary with the results that you can convert into a dataframe like this:\n",
    "\n",
    "```py\n",
    "report = classification_report(..., output_dict=True)\n",
    "report pd.DataFrame(report).transpose()\n",
    "display(report)\n",
    "```\n",
    "\n",
    "The last three rows of the report are aggregate statistics that you can ignore.\n",
    "\n",
    "Finally, calculate the standard deviation of the page loading time and add it to the report. Use the `sort_values` method of the dataframe to sort the dataframe by the loading time and show only the columns corresponding to _recall_ and the standard deviation of the loading time.\n",
    "\n",
    "Do this only for the Tor dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import classification_report\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "\n",
    "# TODO: obtain train and test splits\n",
    "\n",
    "\n",
    "# TODO: train classifier and predict classes for the test set\n",
    "\n",
    "\n",
    "# TODO: display report\n",
    "\n",
    "\n",
    "# TODO: add and show the load time's stdev next to recall\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Padding schemes\n",
    "\n",
    "**Exercise 6**\n",
    "\n",
    "In this exercise, you will implement and evaluate a padding scheme as a defensive mechanism to protect against our website fingerprinting attack. Padding will be simulated on the data. That means that you will have to implement a method that takes a traffic dataframe and returns the dataframe with the added _padding_. The _padding_ scheme is the following:\n",
    "\n",
    " - **_Padding_ to the next multiple**: given a parameter, $\\alpha$, indicating a fixed number of packets, we add padding packets to each trace until we reach the next multiple of $\\alpha$. For example, if $\\alpha=200$ and there are 345 packets in the trace, we need to add 145 padding packets to it to reach 400 packets.\n",
    " \n",
    "As a result, the scheme is creating _anonymity sets_ with respect to the feature `number of packets in the trace`: pages that had similar but different numbers of packets end up having the same number of packets.\n",
    " \n",
    "For the simulation we will make the following assumptions:\n",
    "\n",
    "  - We assume that we also add padding at a packet level such that **all the packets in the trace** has the same length: 1460 bytes.\n",
    "\n",
    "  - We only add **incoming** padding packets.\n",
    "\n",
    " - To simulate the timestamps of the padding packets, we will assume that the _padding_ timestamps follow a [`Beta`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.beta.html) distribution with its parameters estimated via Maximum Likelihood Estimation from the data:\n",
    "\n",
    "```py\n",
    "# MLE estimate from data\n",
    "delays = tor.timestamp.diff()\n",
    "params = beta.fit(delays.dropna())\n",
    "```\n",
    "\n",
    "You do not need to estimate the parameters of this Beta distribution, use the ones provided in the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.stats import beta\n",
    "\n",
    "\n",
    "PACKET_SIZE      = 1460\n",
    "TIMESTAMP_PARAMS = (0.6961851058342148, 172.0499069713989, 9.536743164062499e-07, 5.721616484081675)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**a.** Implement the methods to simulate the padding packets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: implement padding method:\n",
    "def next_multiple_padding(trace: pd.DataFrame, a: int) -> pd.DataFrame:\n",
    "    \"\"\"Return the padded trace.\"\"\"\n",
    "    # TODO\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**b.** Apply the padding scheme where $\\alpha$ is the maximum number of packets across all traces in the Tor dataset **plus 1**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: apply padding to tor dataframe:\n",
    "# Hint: use the apply method on the dataframe `groupby`.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**c.** Extract the features and show the resulting dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#TODO: extract features and show dataframe:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CHECKPOINT: load the intermediate result\n",
    "tor_features_padded = pd.read_csv('intermediate/ex6_tor_padded_features.csv', index_col=False)\n",
    "tor_features_padded.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**d.** Train and test the decision tree with the new features set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: train and test the decision tree classifier with the features from the padded data:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overhead metrics\n",
    "\n",
    "**Exercise 7**\n",
    "\n",
    "If $X$ is the original dataset, $X'$ is the padded dataset, and $f$ is the method that measures a communication performance property of the data (e.g., latency, volume, bandwidth). Then, the communication [_overhead_](https://en.wikipedia.org/wiki/Overhead_(computing)) is defined as:\n",
    "\n",
    "$\\mbox{Overhead(f, X, X')} = \\frac{f(X')}{f(X)}$\n",
    "\n",
    "**a.** Implement the method to calculate the _overhead_ when $f$ measures:\n",
    "\n",
    " 1. `overhead_volume`: Incoming traffic volume in bytes\n",
    " 2. `overhead_latency`: Latency (loading time)\n",
    "\n",
    "**Hint**: Assume that the dataframes that these methods take are the feature dataframes, and not the traffic dataframes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: implement the overhead methods:\n",
    "\n",
    "def overhead_volume(original: pd.DataFrame, padded: pd.DataFrame) -> float:\n",
    "    \"\"\"Return of volume overhead of padded over original.\"\"\"\n",
    "    #TODO\n",
    "    pass\n",
    "\n",
    "\n",
    "def overhead_latency(original: pd.DataFrame, padded: pd.DataFrame) -> float:\n",
    "    \"\"\"Return letency overhead of padded over original.\"\"\"\n",
    "    #TODO\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**b.** Calculate the _overheads_ for the padding scheme of Exercise 6."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: ompute overheads:\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "latex_envs": {
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 0
  },
  "toc": {
   "toc_cell": true,
   "toc_number_sections": true,
   "toc_threshold": 6,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
