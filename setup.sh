#!/bin/bash

# check python3 is installed
python3 -V > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "python3 not installed"
fi

# ensure pip is installed
python3 -m ensurepip

# install virtualenv
pip3 install virtualenv

# create virtual environment
virtualenv -p /usr/bin/python3 .venv

# activate environment
source activate .venv

# install required modules
python3 -m pip install -r requirements.txt

# unzip data
unzip 'data/*.zip' -d data/

# run jupyter notebook
jupyter-notebook lab1.ipynb

